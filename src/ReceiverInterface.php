<?php

namespace PeterNikonov\WinPay;

interface ReceiverInterface
{
    /**
     * Set source value.
     *
     * @param $resource
     * @return mixed
     */
    public function setResource($resource);
}
