<?php

namespace PeterNikonov\WinPay;

class TextBlock
{
    /**
     * @var string $text
     */
    protected $text = '';
    /**
     * @var array $replace
     */
    protected $replace = [];

    public function __construct(string $text = null)
    {
        if ($text) {
            $this->setText($text);
        }
    }

    function __toString()
    {
        return $this->getText();
    }

    /**
     * @return array
     */
    public function getReplace(): array
    {
        return $this->replace;
    }

    /**
     * Set replace array
     * @param array $replace
     * @return self
     */
    public function setReplace(array $replace): self
    {
        $this->replace = $replace;
        return $this;
    }

    /**
     * @param array $pair
     * @return self
     */
    public function addPair(array $pair): self
    {
        $this->replace[] = $pair;
        return $this;
    }

    /**
     * @return self
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        $this->replaceWithArray($this->getReplace());
        return $this->text;
    }

    /**
     * Replace values
     *
     * @param array $array
     */
    public function replaceWithArray(array $array)
    {
        foreach ($array as $search => $replace) {
            if (is_array($replace)) {
                $this->replaceWithArray($replace);
            } else {
                $this->text = str_replace($search, $replace, $this->text);
            }
        }
    }
}
