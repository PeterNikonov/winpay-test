<?php

namespace PeterNikonov\WinPay;

use GuzzleHttp\Client as HttpClient;

class UrlReceiver implements ReceiverInterface
{
    /**
     * @var string string
     */
    protected $resource;
    /**
     * @var Client
     */
    protected $client;

    /**
     * Default timeout value
     */
    const TIMEOUT = 10;
    /**
     * Default connect timeout value
     */
    const CONNECT_TIMEOUT = 10;

    public function __construct($resource = '', HttpClient $client) {
        $this->resource = $resource;
        $this->client = $client;
    }

    /**
     * @inheritdoc
     * @param $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Receive data from setted resource.
     *
     * @return \Psr\Http\Message\StreamInterface|void
     */
    public function getContent() {
        try {
            $request = $this->client->request('GET', $this->resource, ['connect_timeout' => self::CONNECT_TIMEOUT, 'timeout' => self::TIMEOUT]);
            return $request->getBody();
        } catch (\Exception $e) {
            return;
        }
    }
}
