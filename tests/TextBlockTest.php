<?php

use PeterNikonov\WinPay\TextBlock;
use PHPUnit\Framework\TestCase;

class TextBlockTest extends TestCase
{
    public function test()
    {
        $expected = 'a is One, b is Two, c is Three and options: (login:Peter, pass:123456)... Oh, pair value is available too!';

        $text = 'a is :a, b is :b, c is :c and options: (login::login, pass::pass)... Oh, pair value is :pair';
        $replace = [
            ':a' => 'One',
            ':b' => 'Two',
            ':c' => 'Three',
            'options' => [
                ':login' => 'Peter',
                'subsub' => [
                    'We need to go deeper)))' => [':pass' => '123456']
                ]
            ]
        ];

        $textBlock = new TextBlock($text);
        $textBlock->setReplace($replace);
        $textBlock->addPair([':pair' => 'available too!']);

        $this->assertEquals($expected, $textBlock);
    }
}
