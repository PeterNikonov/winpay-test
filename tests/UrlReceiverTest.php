<?php

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PeterNikonov\WinPay\UrlReceiver;
use PHPUnit\Framework\TestCase;

class UrlReceiverTest extends TestCase
{
    public function testGetContent()
    {
        $hello = 'HELLO';

        $mock = new MockHandler([
            new Response(200, [], $hello),
            new RequestException("Request Exception", new Request('GET', '/')),
            new ConnectException("Connection Exception", new Request('GET', '/')),
        ]);

        $handler = HandlerStack::create($mock);
        $client = new HttpClient(['handler' => $handler]);

        $receiver = new UrlReceiver('', $client);

        $a = $receiver->getContent();
        $b = $receiver->getContent();
        $c = $receiver->getContent();

        $this->assertEquals($hello, $a);
        $this->assertNull($b);
        $this->assertNull($c);

        // Send real request
        $client = new HttpClient();
        $receiver = new UrlReceiver('http://httpbin.org/delay/30', $client);
        $this->assertNull($receiver->getContent());
    }
}
